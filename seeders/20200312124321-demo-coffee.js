'use strict';
const fakerRU = require('faker/locale/ru');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'coffees',
      createSeeds(
        {
          schema: () => ({
            name: fakerRU.internet.name(),
            type: fakerRU.internet.userAgent(),
            shopId: 25,
            createdAt: new Date(),
            updatedAt: new Date(),
          }),
        },
        3
      )
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('coffees', null, {});
  }
};

function createSeeds({ schema }, limit) {
  let counter = 0;
  let array = [];
  while (counter < limit) {
    array.push(schema());
    counter += 1;
  }
  return array;
}
