'use strict';
const fakerRU = require('faker/locale/ru');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'shops',
      createSeeds(
        {
          schema: () => ({
            name: fakerRU.lorem.text(),
            createdAt: new Date(),
            updatedAt: new Date(),
          }),
        },
        1
      )
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('shops', null, {});
  }
};

function createSeeds({ schema }, limit) {
  let counter = 0;
  let array = [];
  while (counter < limit) {
    array.push(schema());
    counter += 1;
  }
  return array;
}
